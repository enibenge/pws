package models;

public class SearchConfiguration
{
    private String searchName;

    private boolean shouldShowResults;

    private boolean shouldShowElapsedTime;

    private int maxNumberOfThreads;

    public String getSearchName()
    {
        return searchName;
    }

    public boolean getShouldShowResults()
    {
        return shouldShowResults;
    }

    public boolean getShouldShowElapsedTime()
    {
        return shouldShowElapsedTime;
    }

    public int getMaxNumberOfThreads()
    {
        return maxNumberOfThreads;
    }

    public void setSearchName(String searchName)
    {
        this.searchName = searchName;
    }

    public void setShouldShowResults(boolean shouldShowResults)
    {
        this.shouldShowResults = shouldShowResults;
    }

    public void setShouldShowElapsedTime(boolean shouldShowElapsedTime)
    {
        this.shouldShowElapsedTime = shouldShowElapsedTime;
    }

    public void setMaxNumberOfThreads(int maxNumberOfThreads)
    {
        this.maxNumberOfThreads = maxNumberOfThreads;
    }

    public SearchConfiguration(boolean shouldShowResults, boolean shouldShowElapsedTime, int maxNumberOfThreads)
    {
        this.searchName = "";
        this.shouldShowResults = shouldShowResults;
        this.shouldShowElapsedTime = shouldShowElapsedTime;
        this.maxNumberOfThreads = maxNumberOfThreads;
    }
}
