package models;

public record TriePrefixSubset(String prefix, ConcurrentTrieNode trieNode)
{
}
