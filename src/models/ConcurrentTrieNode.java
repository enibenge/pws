package models;

import java.util.*;
import java.util.concurrent.*;

public class ConcurrentTrieNode
{
    private final ConcurrentHashMap<Character, ConcurrentTrieNode> children;

    private boolean isWord;

    public Map<Character, ConcurrentTrieNode> getChildren() {
        return children;
    }

    public boolean getIsWord()
    {
        return isWord;
    }

    public void setIsWord() {
        isWord = true;
    }

    public ConcurrentTrieNode()
    {
        children = new ConcurrentHashMap<>();
    }
}
