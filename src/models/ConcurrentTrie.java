package models;

import search.algorithms.TrieSearch;

import java.util.Collections;
import java.util.List;

public class ConcurrentTrie
{
    private final ConcurrentTrieNode root;

    public ConcurrentTrie()
    {
        root = new ConcurrentTrieNode();
    }

    public synchronized void insert(String[] wordsToInsert)
    {
        for (String word : wordsToInsert)
        {
            insert(word);
        }
    }

    public synchronized void insert(String wordToInsert)
    {
        ConcurrentTrieNode current = root;

        for (Character character : wordToInsert.toCharArray())
        {
            current = current.getChildren().computeIfAbsent(character, trieNode -> new ConcurrentTrieNode());
        }

        current.setIsWord();
    }

    public synchronized ConcurrentTrieNode searchForTrieNodeMatchingSearchTerm(String searchTerm)
    {
        ConcurrentTrieNode current = root;
        for (int i = 0; i < searchTerm.length(); i++)
        {
            char c = searchTerm.charAt(i);
            ConcurrentTrieNode node = current.getChildren().get(c);
            if (node == null)
            {
                return null;
            }
            current = node;
        }

        return current;
    }

    public List<String> search(String searchPrefix)
    {
        var trieNodeMatchingPrefix = searchForTrieNodeMatchingSearchTerm(searchPrefix);
        if (trieNodeMatchingPrefix == null)
        {
            return Collections.emptyList();
        }
        return TrieSearch.findAllWordsBelowTrieNode(new TriePrefixSubset(searchPrefix, trieNodeMatchingPrefix));
    }
}
