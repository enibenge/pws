package utility;

public class Stopwatch
{
    private long time;

    public void start()
    {
        time = System.currentTimeMillis();
    }

    public void stop()
    {
        time = System.currentTimeMillis() - time;
    }

    public long getElapsedMilliseconds()
    {
        return time;
    }

    public Stopwatch(boolean autoStart)
    {
        if (autoStart)
        {
            start();
        }
    }
}
