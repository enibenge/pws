package utility;

import java.util.*;

public class WordListGenerator
{
    public static String[] generateCartesianProductOfAlphabet(int length)
    {
        return generateCartesianProductOfAlphabet("", length).toArray(new String[0]);
    }

    private static List<String> generateCartesianProductOfAlphabet(String input, int length)
    {
        List<String> result = new ArrayList<>();

        if (length == 0)
        {
            result.add(input);
            return result;
        }
        for (char c = 'A'; c <= 'Z'; c++)
        {
            result.addAll(generateCartesianProductOfAlphabet(input + c, length - 1));
        }

        return result;
    }
}
