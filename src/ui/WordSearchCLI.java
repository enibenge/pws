package ui;

import models.SearchConfiguration;
import search.SearchExecutor;
import utility.WordListGenerator;

import java.util.Optional;
import java.util.Scanner;

public class WordSearchCLI implements WordSearchInterface {
    private static final int WORDS_LENGTH_FOUR = 4;
    private static final int WORDS_LENGTH_FIVE = 5;
    private static final int MAX_NUMBER_OF_THREADS = 8;
    private static final int MULTIPLE_EXECUTIONS = 25;

    private static final SearchConfiguration searchConfiguration = new SearchConfiguration(true, true, MAX_NUMBER_OF_THREADS);
    private static String[] searchableList = WordListGenerator.generateCartesianProductOfAlphabet(WORDS_LENGTH_FOUR);

    private static Optional<String> savedCommand = Optional.empty();

    SearchExecutor searchExecutor;

    public WordSearchCLI(SearchExecutor searchExecutor)
    {
        this.searchExecutor = searchExecutor;
    }

    public void showInterface()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.println("<<< Word Search Client has started >>>");

        while(true)
        {
            System.out.println("Please type your search command:");
            System.out.print("> ");

            var input = scanner.nextLine().toLowerCase();
            if (input.equals("x"))
            {
                return;
            }
            else if (input.equals("reset"))
            {
                savedCommand = Optional.empty();
            }

            var command = savedCommand.orElse(input);
            switch (command)
            {
                case "exit":
                    return;

                case "set":
                    savedCommand = Optional.of(scanner.nextLine().toLowerCase());
                    break;

                case "sc":
                case "show config":
                    printConfig();
                    break;
                case "ec":
                case "edit config":
                    editConfig(scanner);
                    break;

                case "wfour":
                    searchableList = WordListGenerator.generateCartesianProductOfAlphabet(WORDS_LENGTH_FOUR);
                    System.out.println("Use list of words with length 4 (AAAA - ZZZZ).");
                    break;
                case "wfive":
                    searchableList = WordListGenerator.generateCartesianProductOfAlphabet(WORDS_LENGTH_FIVE);
                    System.out.println("Use list of words with length 5 (AAAAA - ZZZZZ).");
                    break;

                case "ls":
                case "linear search":
                    System.out.println("linear search");
                    searchExecutor.executeLinearSearch(searchableList, promptForSearch(scanner), searchConfiguration);
                    break;
                case "dls":
                case "distributed linear search":
                    System.out.println("distributed linear search");
                    searchExecutor.executeDistributedLinearSearch(searchableList, promptForSearch(scanner), searchConfiguration);
                    break;

                case "ts":
                case "trie search":
                case "build trie and search":
                    System.out.println("build trie and search");
                    searchExecutor.buildTrieAndExecuteSearch(searchableList, promptForSearch(scanner), searchConfiguration);
                    break;
                case "ets":
                case "trie search on existing tree":
                case "existing trie search":
                    System.out.println("search on existing tree");
                    searchExecutor.executeSearchOnExistingTrie(promptForSearch(scanner), searchConfiguration);
                    break;
                case "dets":
                case "distributed trie search on existing tree":
                case "distributed existing trie search":
                    System.out.println("distributed search on existing tree");
                    searchExecutor.executeDistributedSearchOnExistingTrie(promptForSearch(scanner), searchConfiguration);
                    break;
                case "mets":
                case "multiple search on existing tree":
                    System.out.println("multiple search on existing tree");
                    searchExecutor.executeMultipleSearchesOnExistingTrie(promptForSearch(scanner), MULTIPLE_EXECUTIONS, searchConfiguration);
                    break;
                case "mdets":
                case "multiple distributed search on existing tree":
                    System.out.println("multiple search on existing tree");
                    searchExecutor.executeMultipleDistributedSearchesOnExistingTrie(promptForSearch(scanner), MULTIPLE_EXECUTIONS, searchConfiguration);
                    break;
                case "bdts":
                case "build trie distributed and search":
                    System.out.println("build trie distributed and search");
                    searchExecutor.buildTrieDistributedAndExecuteSearch(searchableList, promptForSearch(scanner), searchConfiguration);
                    break;

                case "dialog":
                    SearchDialog.makeFrame();
            }
        }
    }

    private void printConfig()
    {
        System.out.println("Current search configuration:");
        System.out.printf("maxNumberOfThreads: %d\n", searchConfiguration.getMaxNumberOfThreads());
        System.out.printf("shouldShowResults: %b\n", searchConfiguration.getShouldShowResults());
        System.out.printf("shouldShowElapsedTime: %b\n", searchConfiguration.getShouldShowElapsedTime());
    }

    private void editConfig(Scanner scanner)
    {
        System.out.println("Please configure search configuration:");
        searchConfiguration.setMaxNumberOfThreads(promptForMaxThreads(scanner));
        System.out.println("Show results after search has finished? (true or false)");
        searchConfiguration.setShouldShowResults(promptForBoolean(scanner));
        System.out.println("Show elapsed time after search has finished? (true or false)");
        searchConfiguration.setShouldShowElapsedTime(promptForBoolean(scanner));
    }

    private String promptForSearch(Scanner scanner)
    {
        System.out.println("Please enter the search prefix:");
        System.out.print("> ");
        return scanner.nextLine().toUpperCase();
    }

    private int promptForMaxThreads(Scanner scanner)
    {
        System.out.printf("This machine seems to have %d processors\n",Runtime.getRuntime().availableProcessors());
        System.out.println("Enter the maximum number of threads used for the search (int):");
        int result = 1;
        try
        {
            System.out.print("> ");
            result = Integer.parseInt(scanner.nextLine());
        }
        catch (NumberFormatException nfe)
        {
            System.out.println("Cannot parse input, will set maximum number of threads to 1.");
        }

        return result;
    }

    private boolean promptForBoolean(Scanner scanner)
    {
        System.out.print("> ");
        return Boolean.parseBoolean(scanner.nextLine());
    }
}
