package ui;

import search.algorithms.LinearSearch;
import utility.WordListGenerator;

import javax.swing.*;
import java.awt.*;

public class SearchDialog
{
    public static void makeFrame()
    {
        String[] longWordList = WordListGenerator.generateCartesianProductOfAlphabet(4);

        JFrame frame = new JFrame();

        JButton button = new JButton("Search");
        JCheckBox fastSearch = new JCheckBox("Fast search");
        JTextField searchField = new JTextField();

        button.setBounds(200, 490, 100, 40);
        fastSearch.setBounds(150, 10, 200, 40);

        searchField.setBounds(225, 450, 50, 25);
        searchField.setBackground(Color.CYAN);


        frame.add(button);
        frame.add(fastSearch);
        frame.add(searchField);

        frame.setSize(500, 600);

        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        button.addActionListener(arg -> {
            JProgressBar progressBar = new JProgressBar();
            progressBar.setBounds(200, 300, 100, 40);
            frame.add(progressBar);

            if (fastSearch.isSelected())
            {
                LinearSearch.linearSearch(longWordList, searchField.getText());
            }
            else
            {
                LinearSearch.slowLinearSearch(longWordList, searchField.getText(), 4000);
            }
        });
    }
}
