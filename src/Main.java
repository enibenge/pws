import search.SearchExecutor;
import ui.WordSearchCLI;
import ui.WordSearchInterface;

public class Main
{
    public static void main(String[] args)
    {
        WordSearchInterface wordSearchInterface = new WordSearchCLI(new SearchExecutor());
        wordSearchInterface.showInterface();
    }
}