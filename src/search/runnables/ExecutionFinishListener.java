package search.runnables;

import java.util.List;

public interface ExecutionFinishListener
{
    void onFinish(List<String> result);
}
