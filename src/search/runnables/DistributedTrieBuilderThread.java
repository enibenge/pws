package search.runnables;

import models.ConcurrentTrie;

public class DistributedTrieBuilderThread implements Runnable
{
    private final String[] listOfWords;

    private final ConcurrentTrie trie;

    public DistributedTrieBuilderThread(String[] listOfWords, ConcurrentTrie trie)
    {
        this.listOfWords = listOfWords;
        this.trie = trie;
    }

    public void run()
    {
        trie.insert(listOfWords);
    }
}
