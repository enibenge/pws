package search.runnables;

import search.algorithms.LinearSearch;

public class DistributedLinearSearchThread implements Runnable
{
    private final String[] searchableList;
    private final String searchString;
    private final ExecutionFinishListener listener;

    public DistributedLinearSearchThread(String[] searchableList, String searchString, ExecutionFinishListener listener)
    {
        this.searchableList = searchableList;
        this.searchString = searchString;
        this.listener = listener;
    }

    public void run()
    {
        var searchResult = LinearSearch.linearSearch(searchableList, searchString);
        listener.onFinish(searchResult);
    }
}
