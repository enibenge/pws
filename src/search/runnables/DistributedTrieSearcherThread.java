package search.runnables;

import models.TriePrefixSubset;
import search.algorithms.TrieSearch;

public class DistributedTrieSearcherThread implements Runnable
{
    private final TriePrefixSubset triePrefixSubset;

    private final ExecutionFinishListener listener;

    public DistributedTrieSearcherThread(TriePrefixSubset triePrefixSubset, ExecutionFinishListener listener)
    {
        this.triePrefixSubset = triePrefixSubset;
        this.listener = listener;
    }

    public void run()
    {
        listener.onFinish(TrieSearch.findAllWordsBelowTrieNode(triePrefixSubset));
    }
}
