package search;

import models.ConcurrentTrie;
import models.ConcurrentTrieNode;
import models.SearchConfiguration;
import models.TriePrefixSubset;
import search.algorithms.LinearSearch;
import search.runnables.DistributedLinearSearchThread;
import search.runnables.DistributedTrieBuilderThread;
import search.runnables.DistributedTrieSearcherThread;
import utility.Stopwatch;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SearchExecutor
{
    private ConcurrentTrie trie;

    public static final String LINEAR_SEARCH = "Linear search";
    public static final String DISTRIBUTED_LINEAR_SEARCH = "Distributed linear search";
    public static final String TRIE_SEARCH = "Trie search";
    public static final String TRIE_SEARCH_WITH_DISTRIBUTED_BUILD = "Search with distributed Trie build";
    public static final String TRIE_SEARCH_ON_EXISTING_TRIE = "Search on existing Trie";
    public static final String DISTRIBUTED_SEARCH_ON_EXISTING_TRIE = "Distributed search on existing Trie";
    public static final String MULTIPLE_DISTRIBUTED_SEARCHES_ON_EXISTING_TRIE = "Multiple distributed searches on existing Trie";
    public static final String MULTIPLE_SEARCHES_ON_EXISTING_TRIE = "Multiple searches on existing Trie";

    public static final int TIMEOUT_SECONDS = 15;

    public SearchExecutor()
    {
        trie = new ConcurrentTrie();
    }

    public void executeLinearSearch(String[] listToSearch, String searchPrefix, SearchConfiguration searchConfiguration)
    {
        searchConfiguration.setSearchName(LINEAR_SEARCH);
        printStartOfSearch(searchPrefix, searchConfiguration);

        Stopwatch stopwatch = new Stopwatch(true);
        var searchResult = LinearSearch.linearSearch(listToSearch, searchPrefix);
        stopwatch.stop();

        printResults(searchConfiguration, searchResult, stopwatch.getElapsedMilliseconds());
    }

    public void executeDistributedLinearSearch(String[] listToSearch, String searchPrefix, SearchConfiguration searchConfiguration)
    {
        searchConfiguration.setSearchName(DISTRIBUTED_LINEAR_SEARCH);
        printStartOfSearch(searchPrefix, searchConfiguration);

        List<String> result = new ArrayList<>();
        List<String> synchronizedResult = Collections.synchronizedList(result);

        Stopwatch stopwatch = new Stopwatch(true);
        int maxNumberOfThreads = searchConfiguration.getMaxNumberOfThreads();
        ExecutorService executorService = Executors.newFixedThreadPool(maxNumberOfThreads);

        var dividedSearchList = tryDivideSearchListEvenly(listToSearch, maxNumberOfThreads);
        if (dividedSearchList.isEmpty())
        {
            System.out.printf("Searchable list is not equally divisible by number of threads (%d), please choose different number of threads.%n", maxNumberOfThreads);
            return;
        }

        dividedSearchList.forEach(splitSearchableList -> executorService.execute(new DistributedLinearSearchThread(splitSearchableList, searchPrefix, synchronizedResult::addAll)));
        executorService.shutdown();
        try
        {
            if (executorService.awaitTermination(TIMEOUT_SECONDS, TimeUnit.SECONDS))
            {
                stopwatch.stop();
                printResults(searchConfiguration, synchronizedResult, stopwatch.getElapsedMilliseconds());
            }
        }
        catch (InterruptedException ie)
        {
            System.out.println("Distributed linear search did not finish after 15 seconds.");
        }
    }

    public void buildTrieDistributedAndExecuteSearch(String[] listToSearch, String searchPrefix, SearchConfiguration searchConfiguration)
    {
        searchConfiguration.setSearchName(TRIE_SEARCH_WITH_DISTRIBUTED_BUILD);
        printStartOfSearch(searchPrefix, searchConfiguration);

        Stopwatch stopwatch = new Stopwatch(true);
        int maxNumberOfThreads = searchConfiguration.getMaxNumberOfThreads();
        ExecutorService executorService = Executors.newFixedThreadPool(maxNumberOfThreads);

        var dividedSearchList = tryDivideSearchListEvenly(listToSearch, maxNumberOfThreads);
        if (dividedSearchList.isEmpty())
        {
            System.out.printf("Searchable list is not equally divisible by number of threads (%d), please choose different number of threads.%n", maxNumberOfThreads);
            return;
        }

        dividedSearchList.forEach(splitSearchableList -> executorService.execute(new DistributedTrieBuilderThread(splitSearchableList, trie)));
        executorService.shutdown();
        try
        {
            if (executorService.awaitTermination(TIMEOUT_SECONDS, TimeUnit.SECONDS))
            {
                var searchResult = trie.search(searchPrefix);
                stopwatch.stop();
                printResults(searchConfiguration, searchResult, stopwatch.getElapsedMilliseconds());
            }
        }
        catch (InterruptedException ie)
        {
            System.out.printf("Distributed building of Trie did not finish after %d seconds.\n", TIMEOUT_SECONDS);
        }
    }

    public void buildTrieAndExecuteSearch(String[] wordList, String searchPrefix, SearchConfiguration searchConfiguration)
    {
        searchConfiguration.setSearchName(TRIE_SEARCH);
        printStartOfSearch(searchPrefix, searchConfiguration);

        Stopwatch stopwatch = new Stopwatch(true);
        buildTrie(wordList);
        var searchResult = trie.search(searchPrefix);
        stopwatch.stop();

        printResults(searchConfiguration, searchResult, stopwatch.getElapsedMilliseconds());
    }

    public void executeSearchOnExistingTrie(String searchPrefix, SearchConfiguration searchConfiguration)
    {
        searchConfiguration.setSearchName(TRIE_SEARCH_ON_EXISTING_TRIE);
        printStartOfSearch(searchPrefix, searchConfiguration);

        Stopwatch stopwatch = new Stopwatch(true);
        var searchResult = trie.search(searchPrefix);
        stopwatch.stop();

        printResults(searchConfiguration, searchResult, stopwatch.getElapsedMilliseconds());
    }

    public void executeDistributedSearchOnExistingTrie(String searchPrefix, SearchConfiguration searchConfiguration)
    {
        searchConfiguration.setSearchName(DISTRIBUTED_SEARCH_ON_EXISTING_TRIE);
        printStartOfSearch(searchPrefix, searchConfiguration);

        Stopwatch stopwatch = new Stopwatch(true);
        var searchResult = distributedSearchOnExistingTrie(searchPrefix, searchConfiguration);
        stopwatch.stop();
        printResults(searchConfiguration, searchResult, stopwatch.getElapsedMilliseconds());
    }

    public void executeMultipleSearchesOnExistingTrie(String searchPrefix, int executions, SearchConfiguration searchConfiguration)
    {
        List<String> searchResult = new ArrayList<>();
        searchConfiguration.setSearchName(MULTIPLE_SEARCHES_ON_EXISTING_TRIE);
        printStartOfSearch(searchPrefix, searchConfiguration);

        Stopwatch stopwatch = new Stopwatch(true);
        for (int i = 0; i < executions; i++)
        {
            searchResult.addAll(trie.search(searchPrefix));
        }
        stopwatch.stop();
        printResults(searchConfiguration, searchResult, stopwatch.getElapsedMilliseconds());
    }

    public void executeMultipleDistributedSearchesOnExistingTrie(String searchPrefix, int executions, SearchConfiguration searchConfiguration)
    {
        List<String> searchResult = new ArrayList<>();
        searchConfiguration.setSearchName(MULTIPLE_DISTRIBUTED_SEARCHES_ON_EXISTING_TRIE);
        printStartOfSearch(searchPrefix, searchConfiguration);

        Stopwatch stopwatch = new Stopwatch(true);
        for (int i = 0; i < executions; i++)
        {
            searchResult.addAll(distributedSearchOnExistingTrie(searchPrefix, searchConfiguration));
        }
        stopwatch.stop();

        printResults(searchConfiguration, searchResult, stopwatch.getElapsedMilliseconds());
    }

    private List<String> distributedSearchOnExistingTrie(String searchPrefix, SearchConfiguration searchConfiguration)
    {
        var trieNodeMatchingPrefix = trie.searchForTrieNodeMatchingSearchTerm(searchPrefix);
        if (trieNodeMatchingPrefix == null)
        {
            return Collections.emptyList();
        }

        List<String> result = new ArrayList<>();
        List<String> synchronizedResult = Collections.synchronizedList(result);

        if (trieNodeMatchingPrefix.getChildren() != null)
        {
            ExecutorService executorService = Executors.newFixedThreadPool(searchConfiguration.getMaxNumberOfThreads());
            for (Map.Entry<Character, ConcurrentTrieNode> child : trieNodeMatchingPrefix.getChildren().entrySet())
            {
                executorService.execute(new DistributedTrieSearcherThread(new TriePrefixSubset(searchPrefix + child.getKey(), child.getValue()), synchronizedResult::addAll));
            }
            executorService.shutdown();
            try
            {
                if (executorService.awaitTermination(TIMEOUT_SECONDS, TimeUnit.SECONDS))
                {
                    return synchronizedResult;
                }
            }
            catch (InterruptedException ie)
            {
                System.out.println("Distributed linear search did not finish after 15 seconds.");
            }
        }

        return synchronizedResult;
    }

    private List<String[]> tryDivideSearchListEvenly(String[] searchList, int divider)
    {
        List<String[]> result = new ArrayList<>();

        if (searchList.length % divider != 0)
        {
            return Collections.emptyList();
        }

        int dividedLength = searchList.length / divider;

        for (int i = 0; i < divider; i++) {
            int splitStart = dividedLength * i;
            int splitEnd = dividedLength * (i + 1);
            result.add(Arrays.copyOfRange(searchList, splitStart, splitEnd));
        }

        return result;
    }

    private void buildTrie(String[] words)
    {
        trie = new ConcurrentTrie();
        trie.insert(words);
    }

    private void printStartOfSearch(String searchPrefix, SearchConfiguration searchConfiguration)
    {
        System.out.printf("Starting %s with prefix: \"%s\".\n", searchConfiguration.getSearchName(), searchPrefix);
    }

    private void printResults(SearchConfiguration searchConfiguration, List<String> result, long elapsedTime)
    {
        System.out.printf("%s is finished and has found " +  result.size() + " results.\n", searchConfiguration.getSearchName());

        if (searchConfiguration.getShouldShowResults())
        {
            System.out.println(result);
        }

        if (searchConfiguration.getShouldShowElapsedTime())
        {
            System.out.println("Elapsed time : " + elapsedTime + " milliseconds.");
        }

        System.out.println("-------------------------");
    }
}
