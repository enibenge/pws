package search.algorithms;

import models.ConcurrentTrieNode;
import models.TriePrefixSubset;

import java.util.ArrayList;
import java.util.List;

public class TrieSearch
{
    public static List<String> findAllWordsBelowTrieNode(TriePrefixSubset triePrefixSubset)
    {
        List<String> result = new ArrayList<>();

        ConcurrentTrieNode trieNode = triePrefixSubset.trieNode();

        if (trieNode.getIsWord())
        {
            result.add(triePrefixSubset.prefix());
            return result;
        }

        if (trieNode.getChildren() != null)
        {
            trieNode.getChildren().forEach((character, node) -> result.addAll(findAllWordsBelowTrieNode(new TriePrefixSubset(triePrefixSubset.prefix() + character, node))));
        }

        return result;
    }
}
