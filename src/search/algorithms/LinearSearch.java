package search.algorithms;

import java.util.Arrays;
import java.util.List;

public class LinearSearch {
    public static List<String> linearSearch(String[] input, String searchString)
    {
        return Arrays.stream(input).filter(word -> word.startsWith(searchString)).toList();
    }

    public static List<String> slowLinearSearch(String[] input, String searchString, long sleepMillis)
    {
        try
        {
            Thread.sleep(sleepMillis);
        }
        catch (InterruptedException ie)
        {
            System.out.println("E");
        }

        return Arrays.stream(input).filter(word -> word.startsWith(searchString)).toList();
    }
}
